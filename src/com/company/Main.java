package com.company;

import prototype.UnitMilitarySoldier;
import prototype.UnitMilitarySoldierCashe;

public class Main
{

	public static void main(String[] args)
	{
		UnitMilitarySoldierCashe.loadCashe();

		UnitMilitarySoldier cloneUMSoldier1 = (UnitMilitarySoldier) UnitMilitarySoldierCashe.getUnitMilitarySoldier("sub");
		UnitMilitarySoldier cloneUMSoldier2 = (UnitMilitarySoldier) UnitMilitarySoldierCashe.getUnitMilitarySoldier("platoon");
		UnitMilitarySoldier cloneUMSoldier3 = (UnitMilitarySoldier) UnitMilitarySoldierCashe.getUnitMilitarySoldier("coy");
		UnitMilitarySoldier cloneUMSoldier4 = (UnitMilitarySoldier) UnitMilitarySoldierCashe.getUnitMilitarySoldier("battalion");
		UnitMilitarySoldier cloneUMSoldier5 = (UnitMilitarySoldier) UnitMilitarySoldierCashe.getUnitMilitarySoldier("regiment");
		UnitMilitarySoldier cloneUMSoldier6 = (UnitMilitarySoldier) UnitMilitarySoldierCashe.getUnitMilitarySoldier("division");

		System.out.println("Количество солдат в подразделении: " + cloneUMSoldier1.getSoldiers());
		System.out.println("Количество солдат в взводе: " + cloneUMSoldier2.getSoldiers());
		System.out.println("Количество солдат в роте: " + cloneUMSoldier3.getSoldiers());
		System.out.println("Количество солдат в батальоне: " + cloneUMSoldier4.getSoldiers());
		System.out.println("Количество солдат в полку: " + cloneUMSoldier5.getSoldiers());
		System.out.println("Количество солдат в дивизии: " + cloneUMSoldier6.getSoldiers());
	}
}
