package prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class Coy extends UnitMilitaryAmmunition
{
	private List<Platoon> platoons = new ArrayList<>();

	public Coy()
	{
		super(0, 0);
	}

	public void addPlatoon(Platoon platoon)
	{
		this.platoons.add(platoon);
	}

	@Override
	public int getSoldiers()
	{
		int soldiers = 0;

		for (Platoon pl: platoons)
		{
			soldiers += pl.getSoldiers();
		}

		return soldiers;
	}

	@Override
	public int getAmmunitions()
	{
		return super.getAmmunitions();
	}
}
