package prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class Division extends UnitMilitaryEnginery
{
	private List<Regiment> regiments = new ArrayList<>();

	public Division()
	{
		super(0, 0, 0);
	}

	public void addRegiment(Regiment regiment)
	{
		regiments.add(regiment);
	}

	@Override
	public int getSoldiers()
	{
		int sol = 0;

		for (Regiment reg: regiments)
		{
			sol += reg.getSoldiers();
		}

		return sol;
	}

	@Override
	public int getAmmunitions()
	{
		int amm = 0;

		for (Regiment reg: regiments)
		{
			amm += reg.getAmmunitions();
		}

		return amm;
	}

	@Override
	public int getEngineries()
	{
		int eng = 0;

		for (Regiment reg: regiments)
		{
			eng += reg.getEngineries();
		}

		return eng;
	}
}
