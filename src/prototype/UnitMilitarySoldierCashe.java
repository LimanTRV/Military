package prototype;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class UnitMilitarySoldierCashe
{
	private static Hashtable<String, UnitMilitarySoldier> soldierMap = new Hashtable<>();

	public static UnitMilitarySoldier getUnitMilitarySoldier(String unit)
	{
		UnitMilitarySoldier cashedUnitMilitarySoldier = soldierMap.get(unit);
		return (UnitMilitarySoldier) cashedUnitMilitarySoldier.clone();
	}

	public static void loadCashe()
	{
		Subdivision subdivision = new Subdivision(5);

		Platoon platoon = new Platoon();
		platoon.addSubdivision(subdivision);
		platoon.addSubdivision(new Subdivision(10));
		platoon.addSubdivision(new Subdivision(10));

		Coy coy = new Coy();
		coy.addPlatoon(platoon);

		Battalion battalion = new Battalion();
		battalion.addCoy(coy);

		Regiment regiment = new Regiment();
		regiment.addBattalion(battalion);

		Division division = new Division();
		division.addRegiment(regiment);

		soldierMap.put("sub", subdivision);
		soldierMap.put("platoon", platoon);
		soldierMap.put("coy", coy);
		soldierMap.put("battalion", battalion);
		soldierMap.put("regiment", regiment);
		soldierMap.put("division", division);
	}
}
