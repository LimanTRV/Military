package prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class Regiment extends UnitMilitaryEnginery
{
	private List<Battalion> battalions = new ArrayList<>();

	public Regiment()
	{
		super(0, 0, 0);
	}

	public void addBattalion(Battalion battalion)
	{
		battalions.add(battalion);
	}

	@Override
	public int getSoldiers()
	{
		int soldiers = 0;

		for (Battalion bat: battalions)
		{
			soldiers += bat.getSoldiers();
		}

		return soldiers;
	}

	@Override
	public int getAmmunitions()
	{
		int ammunitions = 0;

		for (Battalion bat: battalions)
		{
			ammunitions += bat.getAmmunitions();
		}

		return ammunitions;
	}

	@Override
	public int getEngineries()
	{
		int engineries = 0;

		for (Battalion bat: battalions)
		{
			engineries += bat.getEngineries();
		}

		return engineries;
	}
}
