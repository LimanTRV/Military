package prototype;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public abstract class UnitMilitarySoldier implements Cloneable
{
	private int soldiers;

	public UnitMilitarySoldier(int soldiers)
	{
		this.soldiers = soldiers;
	}

	public int getSoldiers()
	{
		return soldiers;
	}

	public Object clone()
	{
		Object clone = null;

		try {
			clone = super.clone();

		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return clone;
	}
}
