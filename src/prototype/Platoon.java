package prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class Platoon extends UnitMilitarySoldier
{
	private List<Subdivision> subdivisions = new ArrayList();

	public Platoon()
	{
		super(0);
	}

	public void addSubdivision(Subdivision subDiv)
	{
		subdivisions.add(subDiv);
	}

	@Override
	public int getSoldiers()
	{
		int soldiers = 0;

		for (Subdivision sub: subdivisions)
		{
			soldiers += sub.getSoldiers();
		}

		return soldiers;
	}
}
