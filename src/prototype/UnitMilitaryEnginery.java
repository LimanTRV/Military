package prototype;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public abstract class UnitMilitaryEnginery extends UnitMilitaryAmmunition
{
	private int engineries;

	public UnitMilitaryEnginery(int soldiers, int ammunitions, int engineries)
	{
		super(soldiers, ammunitions);
		this.engineries = engineries;
	}

	public int getEngineries()
	{
		return engineries;
	}
}
