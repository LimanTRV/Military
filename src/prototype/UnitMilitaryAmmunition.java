package prototype;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public abstract class UnitMilitaryAmmunition extends UnitMilitarySoldier
{
	private int ammunitions;

	public UnitMilitaryAmmunition(int soldiers, int ammunitions)
	{
		super(soldiers);
		this.ammunitions = ammunitions;
	}

	public int getAmmunitions()
	{
		return ammunitions;
	}
}
