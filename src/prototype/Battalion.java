package prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 03.05.2017.
 */
public class Battalion extends UnitMilitaryEnginery
{
	private List<Coy> coies = new ArrayList<>();

	public Battalion()
	{
		super(0, 0, 0);
	}

	public void addCoy(Coy coy)
	{
		coies.add(coy);
	}

	@Override
	public int getSoldiers()
	{
		int soldiers = 0;

		for (Coy coy: coies)
		{
			soldiers += coy.getSoldiers();
		}

		return soldiers;
	}

	@Override
	public int getAmmunitions()
	{
		int ammunitions = 0;

		for (Coy coy: coies)
		{
			ammunitions += coy.getAmmunitions();
		}

		return ammunitions;
	}

	@Override
	public int getEngineries()
	{
		return super.getEngineries();
	}
}
